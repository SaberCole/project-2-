package edu.uprm.ece.icom4035.list;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;











public class SortedCircularDoublyLinkedList<E extends Comparable<E>> implements SortedList<E> {
	private DNode<E> header, curr;
	private int length; 
   
	public SortedCircularDoublyLinkedList() { 
		header = new DNode<E>();
		header.setNext(header);
		header.setPrev(header);
		length = 0;
	}
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new SortedCircularDoublyLinkedListIterator<E>();
	}

	@Override
	public boolean add(E obj) {
		if (obj == null)
			return false; 
		DNode<E> node = new DNode<E>(obj);
		DNode<E> current = this.header.getNext();
		while (current != this.header && current.getElement().compareTo(node.getElement()) < 0) {
			current = current.getNext();
		}
		node.setNext(current);
		node.setPrev(current.getPrev());
		node.getPrev().setNext(node);
		current.setPrev(node);
		this.length++;
		return true;
	}


@Override
public int size() {
	// TODO Auto-generated method stub
	return length;
}
private void cleanLinks(DNode<E> obj){
	obj.getPrev().setNext(obj.getNext());
	obj.getNext().setPrev(obj.getPrev());
	obj.setNext(null);
	obj.setPrev(null);
	length--;
}

@Override
public boolean remove(E obj) {
	// TODO Auto-generated method stub
	curr=header.getNext();

	while(curr!=header){
		if(curr.getElement().equals(obj)){
			cleanLinks(curr);
			return true;
		}
		curr=curr.getNext();
	}
	return false;
}

@Override
public boolean remove(int index) {
	// TODO Auto-generated method stub
	if(index<0|| index>length){
		throw new IndexOutOfBoundsException("Index is not within the size");
	}
	int counter=0;
	DNode<E> current=header.getNext();

	while(current != header){
		if(counter==index){
			cleanLinks(current);

			return true;
		}

		counter++;
		current=current.getNext();
	}
	return false;
}

@Override
public int removeAll(E obj) {
	// TODO Auto-generated method stub
	curr=header.getNext();
	int counter= 0;
	while(curr!=header){
		remove(obj);
	}
	return counter;
}

@Override
public E first() {
	// TODO Auto-generated method stub
	return this.isEmpty() ? null:header.getNext().getElement();
}

@Override
public E last() {
	// TODO Auto-generated method stub
	return this.isEmpty() ? null: header.getPrev().getElement();
}

@Override
public E get(int index) {
	// TODO Auto-generated method stub
	if(index < 0 || index > length){
		throw new IndexOutOfBoundsException("Index is not within the range");
	}
	int counter=0;
	DNode<E> helper = header.getNext();

	while(helper != header){
		if(counter == index){
			return helper.getElement();
		}
		counter++;
		helper=helper.getNext();
	}
	return null;
}

@Override
public void clear() {
	// TODO Auto-generated method stub
	header.setNext(header);
	header.setPrev(header);
	length=0;
}

@Override
public boolean contains(E e) {
	// TODO Auto-generated method stub
	if(e==null || this.size()==0) return false;
	
curr=header.getNext();
	
	for(int i=0; i<this.size();i++){
		if(curr.getElement().equals(e)){
             return true;			
		}
		curr=curr.getNext();
	}

	return false;
}

@Override
public boolean isEmpty() {
	// TODO Auto-generated method stub
	return length == 0;
}

	@Override
	public Iterator<E> iterator(int index) {
		// TODO Auto-generated method stub
		return new SortedCircularDoublyLinkedListIndexIterator<E>(index);
	}

	@Override
	public int firstIndex(E e) {

		curr=header.getNext();
		
		for(int i=0; i<this.size();i++){
			if(curr.getElement().equals(e)){
	             return i;			
			}
			curr=curr.getNext();
		}

		return -1;
	}

	@Override
	public int lastIndex(E e) {

	curr=header.getPrev();
		
		for(int i=this.size()-1; i>0;i--){
			if(curr.getElement().equals(e)){
	             return i;			
			}
			curr=curr.getPrev();
		}

		return -1;
	}


	@Override
	public ReverseIterator<E> reverseIterator() {
		// TODO Auto-generated method stub
		return new SortedCircularDoublyLinkedListReverseIterator<E>();
	}

	@Override
	public ReverseIterator<E> reverseIterator(int index) {
		// TODO Auto-generated method stub
		return new SortedCircularDoublyLinkedListIndexReverseIterator<E>(index);
	}
	
	private static class DNode<E> implements Node<E> {
		private E element; 
		private DNode<E> prev, next; 

		// Constructors
		public DNode() {}

		public DNode(E e) { 
			element = e; 
		}

		public DNode(DNode<E> p, E e, DNode<E> n) { 
			prev = p; 
			next = n; 
			element=e;
		}

		// Methods
		public DNode<E> getPrev() {
			return prev;
		}
		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}
		public DNode<E> getNext() {
			return next;
		}
		public void setNext(DNode<E> next) {
			this.next = next;
		}
		public E getElement() {
			return element; 
		}

		public void setElement(E data) {
			element = data; 
		} 


		public void cleanLinks() { 
			prev = next = null; 
		}

	}

	private class SortedCircularDoublyLinkedListIterator<E> implements Iterator<E>{
		private DNode<E> current;
		
		SortedCircularDoublyLinkedListIterator(){
			current=(DNode<E>) header.getNext();
		}
		
		@Override
		public boolean hasNext(){
			return current!=header;
		}
		public E next(){
			if(hasNext()){
				E result=current.getElement();
				current=current.getNext();
				return result;
			}
			throw new NoSuchElementException();
		}
	}
	private class SortedCircularDoublyLinkedListIndexIterator<E> implements Iterator<E>{
		private DNode<E> current;
		private int position;
		SortedCircularDoublyLinkedListIndexIterator(int index){
			current=(DNode<E>) header.getNext();
			this.position=index;
			if(position<length){
			for(int i=0;i<index;i++){
				current=current.getNext();
			}
			}
		}
		
		@Override
		public boolean hasNext(){
			if(position>length) throw new IndexOutOfBoundsException("Index is out of boundes");
			return current!=header;
		}
		public E next(){
			if(hasNext()){
				E result=current.getElement();
				current=current.getNext();
				position++;
				return result;
			}
			throw new NoSuchElementException();
		}
	}
	
	private class SortedCircularDoublyLinkedListReverseIterator<E> implements ReverseIterator<E>{
		private DNode<E> current;
		
		SortedCircularDoublyLinkedListReverseIterator(){
			current=(DNode<E>) header.getPrev();
		}
		
		@Override
		public boolean hasPrevious(){
			return current!=header;
		}
		public E previous(){
			if(hasPrevious()){
				E result=current.getElement();
				current=current.getPrev();
				return result;
			}
			throw new NoSuchElementException();
		}
	}
	
	private class SortedCircularDoublyLinkedListIndexReverseIterator<E> implements ReverseIterator<E>{
		private DNode<E> current;
		private int position;
		SortedCircularDoublyLinkedListIndexReverseIterator(int index){
			current=(DNode<E>) header.getPrev();
			this.position=index;
			if(position<length){
			for(int i=length;i>index;i--){
				current=current.getPrev();
			}
			}
		}
		
		@Override
		public boolean hasPrevious(){
			if(position>length) throw new IndexOutOfBoundsException("Index is out of bounds");
			return current!=header;
		}
		public E previous(){
			if(hasPrevious()){
				E result=current.getElement();
				current=current.getPrev();
				position--;
				return result;
			}
			throw new NoSuchElementException();
		}
	}
}
